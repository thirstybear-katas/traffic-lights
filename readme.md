# Traffic Light Kata
A coding kata in Java based on Christian Clausen's example from his [GOTO Conference talk in 2022](https://youtu.be/APdaacGmDew).

# Context
This code simulates what happens to a car at traffic lights in the UK (and some other countries!).

Red - car stops<br>
Red & Amber - put car into gear (yes, UK lights have this interim state, effectively "_get ready to pull away_")<br>
Green - drive<br>
Amber - car stops<br> 
...and back to red...

Currently the TrafficLight code is very procedural in nature. The aim is to change the code to be better structured.