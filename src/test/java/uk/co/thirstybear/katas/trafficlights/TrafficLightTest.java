package uk.co.thirstybear.katas.trafficlights;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class TrafficLightTest {
  @Test
  public void carStopsOnRed() {
    Light light = Light.RED;
    StubCar car = new StubCar();

    TrafficLight trafficLight = new TrafficLight();
    trafficLight.updateCar(car, light);
    assertEquals(State.STOPPED, car.getState());
  }

  @Test
  public void carStopsOnAmber() {
    Light light = Light.AMBER;
    StubCar car = new StubCar();

    TrafficLight trafficLight = new TrafficLight();
    trafficLight.updateCar(car, light);
    assertEquals(State.STOPPED, car.getState());
  }

  @Test
  public void carInGearOnRedAmber() {
    Light light = Light.RED_AMBER;
    StubCar car = new StubCar();

    TrafficLight trafficLight = new TrafficLight();
    trafficLight.updateCar(car, light);
    assertEquals(State.IN_GEAR, car.getState());
  }

  @Test
  public void carDrivesOnGreen() {
    Light light = Light.GREEN;
    StubCar car = new StubCar();

    TrafficLight trafficLight = new TrafficLight();
    trafficLight.updateCar(car, light);
    assertEquals(State.DRIVE, car.getState());
  }

  private class StubCar implements Car {
    private State state;

    @Override
    public void drive() {
      state = State.DRIVE;
    }

    @Override
    public void putIntoGear() {
      state = State.IN_GEAR;
    }

    @Override
    public void stop() {
      state = State.STOPPED;
    }

    State getState() {
      return state;
    }
  }

  private enum State {
    DRIVE, IN_GEAR, STOPPED
  }
}