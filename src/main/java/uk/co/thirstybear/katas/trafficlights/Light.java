package uk.co.thirstybear.katas.trafficlights;

public enum Light {
  RED, RED_AMBER, GREEN, AMBER
}
