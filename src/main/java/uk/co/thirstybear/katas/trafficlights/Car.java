package uk.co.thirstybear.katas.trafficlights;

public interface Car {
  void drive();
  void putIntoGear();
  void stop();
}
