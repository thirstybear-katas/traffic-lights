package uk.co.thirstybear.katas.trafficlights;

public class TrafficLight {
  public void updateCar(Car car, Light light) {
    switch (light) {
      case GREEN -> car.drive();
      case RED_AMBER -> car.putIntoGear();
      default -> car.stop();
    }
  }
}